import { Component, OnInit } from '@angular/core';
declare const createMapGraph: any;

@Component({
  selector: 'app-goodsmovement',
  templateUrl: './goodsmovement.component.html',
  styleUrls: ['./goodsmovement.component.css']
})
export class GoodsmovementComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onClick(){
    createMapGraph();
  }

}
