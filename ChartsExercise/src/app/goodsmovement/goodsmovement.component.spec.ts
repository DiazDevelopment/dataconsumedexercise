import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsmovementComponent } from './goodsmovement.component';

describe('GoodsmovementComponent', () => {
  let component: GoodsmovementComponent;
  let fixture: ComponentFixture<GoodsmovementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsmovementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsmovementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
