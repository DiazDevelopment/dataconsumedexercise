import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CurrentriskComponent } from './currentrisk/currentrisk.component';
import { GeneralriskComponent } from './generalrisk/generalrisk.component';
import { RiskovertimeComponent } from './riskovertime/riskovertime.component';
import { GoodsmovementComponent } from './goodsmovement/goodsmovement.component';

@NgModule({
  declarations: [
    AppComponent,
    CurrentriskComponent,
    GeneralriskComponent,
    RiskovertimeComponent,
    GoodsmovementComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
