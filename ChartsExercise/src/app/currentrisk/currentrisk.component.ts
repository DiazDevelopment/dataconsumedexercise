import { Component, OnInit } from '@angular/core';
declare const createPie: any;

@Component({
  selector: 'app-currentrisk',
  templateUrl: './currentrisk.component.html',
  styleUrls: ['./currentrisk.component.css']

})
export class CurrentriskComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

  }

    onClick() {
    createPie();
  }
}
