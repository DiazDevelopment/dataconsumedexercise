import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentriskComponent } from './currentrisk.component';

describe('CurrentriskComponent', () => {
  let component: CurrentriskComponent;
  let fixture: ComponentFixture<CurrentriskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentriskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentriskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
