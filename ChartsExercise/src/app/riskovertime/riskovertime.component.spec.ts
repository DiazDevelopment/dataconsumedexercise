import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskovertimeComponent } from './riskovertime.component';

describe('RiskovertimeComponent', () => {
  let component: RiskovertimeComponent;
  let fixture: ComponentFixture<RiskovertimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskovertimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskovertimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
