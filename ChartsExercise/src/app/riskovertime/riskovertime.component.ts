import { Component, OnInit } from '@angular/core';
declare const createLineChart: any;

@Component({
  selector: 'app-riskovertime',
  templateUrl: './riskovertime.component.html',
  styleUrls: ['./riskovertime.component.css']
})
export class RiskovertimeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onClick(){
    createLineChart();
  }

}
