import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralriskComponent } from './generalrisk.component';

describe('GeneralriskComponent', () => {
  let component: GeneralriskComponent;
  let fixture: ComponentFixture<GeneralriskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralriskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralriskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
