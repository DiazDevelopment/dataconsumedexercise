import { Component, OnInit } from '@angular/core';
declare const createBarChart: any;

@Component({
  selector: 'app-generalrisk',
  templateUrl: './generalrisk.component.html',
  styleUrls: ['./generalrisk.component.css']
})
export class GeneralriskComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onClick(){
    createBarChart();
  }

}
