
function createPie(){

	hideAndShow("currentRiskChartDiv");

		var pieChart = am4core.create("currentRiskChartDiv", am4charts.PieChart);
		pieChart.innerRadius = am4core.percent(60);

		//get the external data 
		pieChart.dataSource.url = "assets/current_risk.json";

		var pieSeries = pieChart.series.push(new am4charts.PieSeries());

		pieSeries.dataFields.category = "current_risk";
		pieSeries.dataFields.value = "average_risk";
		

}

function createLineChart(){

	hideAndShow("riskOverTimeChartDiv");
	
	am4core.useTheme(am4themes_animated);

	var lineChart = am4core.create("riskOverTimeChartDiv", am4charts.XYChart);

	//get the external data
	lineChart.dataSource.url = "assets/risk_overtime.json";

	var dateAxis = lineChart.xAxes.push(new am4charts.DateAxis());
	dateAxis.renderer.minGridDistance = 60;

	var valueAxis = lineChart.yAxes.push(new am4charts.ValueAxis());
	
	renderLine("state_laws_risk", "State laws");
	renderLine("nexus_risk", "Nexus");
	renderLine("data_quality_risk", "Data quality");
	renderLine("current_liabilities_risk", "Current liabilities");
	renderLine("derived_liabilities_risk", "Derived liabilities");
	renderLine("exceptions_risk", "Exceptions");

	function renderLine(value,name){
		
	var series = lineChart.series.push(new am4charts.LineSeries());
	series.dataFields.valueY = value;
	series.dataFields.dateX = "createdDate";
	var bullet = series.bullets.push(new am4charts.Bullet());
	var circle = bullet.createChild(am4core.Circle);
	circle.radius = 5;
	circle.tooltipText = "Risk: {valueY}\n" + "Category: " + name;

	}


}

function createBarChart(){

	hideAndShow("generalRiskDIV");
	
	am4core.useTheme(am4themes_animated);

	var barChart = am4core.create("generalRiskDIV", am4charts.XYChart);
	barChart.dataSource.url = "assets/general_risk.json";

	// Create axes
	var dateAxis = barChart.xAxes.push(new am4charts.DateAxis());
	dateAxis.renderer.grid.template.location = 0;
	dateAxis.renderer.minGridDistance = 30;

	var valueAxis = barChart.yAxes.push(new am4charts.ValueAxis());
	
	renderBar("state_laws_risk", "State laws");
	renderBar("current_liabilities_risk", "Current liabilities");
	renderBar("nexus_risk", "Nexus");
	renderBar("data_quality_risk", "Data quality");  
	renderBar("derived_liabilities_risk", "Derived liabilities");
	renderBar("exceptions_risk","Exceptions");

	function renderBar(value,name){

	var series = barChart.series.push(new am4charts.ColumnSeries());
	series.dataFields.valueY = value;
	series.dataFields.dateX = "currentDate";
  	series.tooltipText = "Risk: " + "{valueY}";
	series.strokeWidth = 2;
	series.name = name;  

	}


	barChart.legend = new am4charts.Legend();
	barChart.cursor = new am4charts.XYCursor();


}



function createMapGraph(){
	
	hideAndShow("goodMovementChartDiv");
	
	var chart = am4core.create("goodMovementChartDiv", am4maps.MapChart);

	chart.geodata = am4geodata_usaLow;

	chart.projection = new am4maps.projections.AlbersUsa();
	var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
	polygonSeries.useGeodata = true;


	var polygonTemplate = polygonSeries.mapPolygons.template;
	polygonTemplate.tooltipText = "{name}";
	polygonTemplate.fill = am4core.color("#74B266");

	
	var imageSeries = chart.series.push(new am4maps.MapImageSeries());


	var imageSeriesTemplate = imageSeries.mapImages.template;
	var circle = imageSeriesTemplate.createChild(am4core.Circle);
	circle.radius = 8;
	circle.fill = am4core.color("#FF9C33");
	circle.stroke = am4core.color("#FFFFFF");
	circle.strokeWidth = 3;
	circle.nonScaling = true;
	circle.tooltipText = "{state}";


	imageSeriesTemplate.propertyFields.latitude = "latitude";
	imageSeriesTemplate.propertyFields.longitude = "longitude";
	imageSeries.dataSource.url = "assets/geoCoord.json";

	var sourceState = new Array();
	var destState= new Array();
	var numTrans = new Array();

	$.ajax({
    async: false,
    url: 'assets/goods_movement.json',
    data: "",
    accepts:'application/json',
    dataType: 'json',
    success: function (data) {
        for (var i = 0; i < data.length; i++) {
            sourceState.push( data[i].source_state );
            destState.push( data[i].destination_state );
            numTrans.push( data[i].num_transactions );
        }
     }
	})

	var latitude = new Array();
	var longitude = new Array();

	$.ajax({
    async: false,
    url: 'assets/geoCoord.json',
    data: "",
    accepts:'application/json',
    dataType: 'json',
    success: function (data) {
        for (var i = 0; i < data.length; i++) {
            latitude.push( data[i].latitude );
            longitude.push( data[i].longitude );
        }
     }
	})


	renderLines(latitude,longitude,0,0,1,sourceState,destState); // CA -> NJ
	renderLines(latitude,longitude,1,0,2,sourceState,destState); // CA -> AZ
	renderLines(latitude,longitude,2,3,2,sourceState,destState); // ND -> AZ
	renderLines(latitude,longitude,3,4,5,sourceState,destState); // WA -> FL
	


	function renderLines(latitude,longitude,index,source,dest,from,to){

		var lineSeries = chart.series.push(new am4maps.MapLineSeries());
		lineSeries.mapLines.template.strokeWidth = numTrans[index];
		lineSeries.mapLines.template.stroke = am4core.color("#E45808");
		lineSeries.tooltipText = "From: "+ from[index] + "\nTo: " + to[index] + "\nTransactions: " + numTrans[index];

		lineSeries.data = [{
  		"multiGeoLine": [
    [
      { "latitude": latitude[source], "longitude": longitude[source] }, //CA
	  { "latitude": latitude[dest], "longitude": longitude[dest] }, //AZ
    	]
  	]
	}];

  }
	
	
}


function hideAndShow(divID){

	var x = document.getElementById(divID);
  	if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }

}